﻿namespace FizzBuzzTests.Services
{
    using System;
    using FizzBuzzWeb.Services;
    using FizzBuzzWeb.Services;
    using FluentAssertions;
    using Moq;
    using Xunit;

    public class FizzRuleTests
    {
        private Mock<IDateTimeService> mockDateTimeService = new Mock<IDateTimeService>();

        [Theory]
        [InlineData(1, false)]
        [InlineData(3, true)]

        public void IsMatchShouldReturnValueBasedOnInput(int input, bool expected)
        {
            // Arrange
            var rule = new FizzRule(this.mockDateTimeService.Object);

            // Act
            var isMatch = rule.IsMatch(input);

            // Assert
            isMatch.Should().Be(expected);
        }

        [Theory]
        [InlineData("14/07/2021", "wizz")]
        [InlineData("19/07/2021", "fizz")]
        public void ExecuteShouldReturnValueBasedOnWednesday(string date, string expected)
        {
            // Arrange
            this.mockDateTimeService.Setup(x => x.GetDateTimeNow()).Returns(Convert.ToDateTime(date));
            var rule = new FizzRule(this.mockDateTimeService.Object);

            // Act
            var execute = rule.Execute();

            // Assert
            execute.Should().Be(expected);
        }
    }
}