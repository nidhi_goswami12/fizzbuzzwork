﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzBuzzWeb.Services
{
    public class FizzBuzzRule : IRule
    {
        public bool IsMatch(int number)
        {
            return number % 15 == 0;
        }
        public string Execute()
        {
            return "fizz buzz";
        }
    }
}