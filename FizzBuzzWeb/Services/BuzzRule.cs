﻿namespace FizzBuzzWeb.Services
{
    using FizzBuzzWeb.Services;
    using System;

    public class BuzzRule : IRule
    {
        private readonly IDateTimeService dateTimeService;

        public BuzzRule(IDateTimeService dateTimeService)
        {
            this.dateTimeService = dateTimeService;
        }

        public bool IsMatch(int number)
        {
            return number % 5 == 0;
        }

        public string Execute()
        {
            var dayOfWeek = this.dateTimeService.GetDateTimeNow().DayOfWeek;
            if (dayOfWeek == DayOfWeek.Wednesday)
            {
                return "wuzz";
            }
            else
            {
                return "buzz";
            }
        }
    }
}