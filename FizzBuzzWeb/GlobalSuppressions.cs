﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "member", Target = "~F:FizzBuzzWeb.Constants.FizzBuzzConstants.PageSize")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Constants.FizzBuzzConstants")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "not required", Scope = "member", Target = "~M:FizzBuzzWeb.Controllers.FizzBuzzWebController.GetFizzBuzzResult(FizzBuzzWeb.Models.FizzbuzzViewModel)~System.Web.Mvc.ActionResult")]
